#!/bin/bash

# Define static variables
jboss="/opt/jboss-4.0.2/server"
jboss8="/opt/wildfly-8.0.0/server"
SSHPASS="`pwd`/sshpass"
user="user"
pass="password"

# Define Error Handlers
function err_handler ()
{
if ! [ "$?" = "0" ]; then
     echo -e "\033[1;0;31m [ERROR]\033[0m: $1 Script Exiting"
     exit 1
    fi
}

function print_ok ()
{
if [ "$?" = "0" ]; then
    echo -e "\033[1;0;32m $1 \033[0m"
fi
}

# Check needed tools
[ -f "$SSHPASS" ] || { echo -e "\033[1;0;31m [ERROR]\033[0m: 'sshpass' executable was NOT found in the current directory. Script Exiting." ; exit 1 ;}
[ -x "$SSHPASS" ] || { chmod +x $SSHPASS; print_ok "[*] Making file 'sshpass' executable"; echo "";}
command -v rsync >/dev/null 2>&1 || { echo >&2 "'rsync' is not installed. Script Exiting."; exit 1; }

# Read input variables
echo ""
read -p "Enter Client's name (ex. metro): " client

# Check if client exists
if  [ -d $jboss/$client ]; then
    print_ok "Found specified client with ST6 installation"
    relst6=1
    jboss="/opt/jboss-4.0.2/server"
elif [ -d $jboss8/$client ]; then
    print_ok "Found specified client with XLR installation"
    relxlr=1
    jboss="/opt/wildfly-8.0.0/server"
else
    echo -e "\033[1;0;31m [ERROR]\033[0m: Cannot find specified client. Script Exiting."
    exit 1
fi

# Extract client release version, check disk space used
function check_client () 
{
if [[ $relst6 = 1 ]]; then
cd $jboss/$client/deploy/main*
release=`pwd | cut -d "/" -f7 | grep -o '[0-9_]*'`
folders="/docs/$client $jboss/$client /www/htdocs/system$release"
systemdocs="/www/htdocs/system$release"
echo ""
echo -e "\033[1;34m [*] Detected release version for $client is: ST $release\033[0m"
echo ""

elif [[ $relxlr = 1 ]]; then
cd $jboss/$client/deployments/core*
release=`pwd | cut -d "/" -f7 | grep -o '[0-9_]*'`
folders="/docs/$client $jboss/$client /www/htdocs/systemxlr8-$release"
systemdocs="/www/htdocs/systemxlr8-$release"
echo ""
echo -e "\033[1;34m [*] Detected release version for $client is: XLR 8.$release\033[0m"
echo ""
fi

echo ---------------------------------------------------
echo -e "\033[1;34m [*] Disk space usage report for $client:\033[0m"
#echo -e "\E[1;33;44m [*] Disk space usage report for $client:\033[0m"
echo ""
for i in $folders
do
echo Client $i folder uses: `du -sh $i | awk '{print $1}'`
done
echo ---------------------------------------------------
echo ""
}

function check_remote_server ()
{
read -p "Enter Destination Server (IP address): " DEST_IP

# Check IP connectivity to remote host
ping -q -w 1 -c 1 $DEST_IP > /dev/null
err_handler "Cannot ping $DEST_IP. It seems dead!"
print_ok "Ping response from $DEST_IP: [OK]"
echo ""
}

function transfer_options ()
{
read -p "Copy system documents? (y/n): " sysdocs
echo ""
echo -e "\E[1;33;44mTransfer options are:\033[0m"
echo "1. RSYNC - recommanded on high speed (LAN) connectivity between servers and big DOCS size"
echo "2. TAR + SCP - recommanded on slow speed connectivity between servers, faster completion time"
echo ""
read -p "Choose transfer method (1/2): " transfer
echo ""

# Initialize counter for script exec-time
before=$(date +%s)
}

#############################################################################################################################################
# Define functions
#############################################################################################################################################

function check-tools() #not used
{
[ -f "$SSHPASS" ] || { echo -e "\033[1;0;31m [ERROR]\033[0m: 'sshpass' executable was NOT found in the current directory. Script Exiting." ; exit 1 ;}
[ -x "$SSHPASS" ] || { chmod +x $SSHPASS; print_ok "[*] Making file 'sshpass' executable"; echo "";}
command -v rsync >/dev/null 2>&1 || { echo >&2 "'rsync' is not installed. Script Exiting."; exit 1; }
}

function check-bkp()
{
# Check for /docs/transfer-temp folder: only used by tar+scp
if [[ $relst6 = 1 ]]
then
backupdir=`date +%Y%m%d`"_"$client"_"st$release
elif [[ $relxlr = 1 ]]
then
backupdir=`date +%Y%m%d`"_"$client"_"xlr8.$release
fi

if [ ! -d /docs/transfer-temp ];
then
        mkdir /docs/transfer-temp
        err_handler "Could not create transfer-temp folder in /docs."
fi
mkdir /docs/transfer-temp/$backupdir
}


function clean-temp()
{
# clean up docs temp files
rm -rf /docs/$client/doc/temp/*;

if [[ $relst6 = 1 ]]; then
# clean up jboss temp files
rm -rf $jboss/$client/tmp/*; rm -rf $jboss/$client/work/*;

elif [[ $relxlr = 1 ]]; then
# clean up jboss temp files
rm -rf $jboss/$client/tmp/*;
fi
}

function rsync-move()
{
echo -e "\033[1;34m [*] Starting to rsync $client files... \033[0m"
echo ""

#rsync system htdocs
if  [ $sysdocs == "y" ]; then

rsync -e "$SSHPASS -p $pass ssh -l $user -o StrictHostKeyChecking=no" -azrhv $systemdocs/ $user@$DEST_IP:$systemdocs/
err_handler "on transferring system documents files."
print_ok "[*] Transfer for system documents completed succesfully."
fi

#rsync jboss client folder and docs
for i in $jboss/$client/ /docs/$client/
do
rsync -e "$SSHPASS -p $pass ssh -l $user -o StrictHostKeyChecking=no" -azrhv $i $user@$DEST_IP:$i
if [ "$?" = 0 ]; then
echo -e "\033[1;0;32m [*] Transfer for $i completed succesfully. \033[0m"
else
echo -e "\033[1;0;31m [ERROR]\033[0m: on transferring $i files. Script Exiting"
exit 1
fi
done
}


function tar-scp()
{
echo ""
echo -e "\033[1;34m [* STEP 1/3 *] Starting archiving files to be moved... \033[0m"
echo ""
# archive docs
cd /docs/
tar -czvf /docs/transfer-temp/$backupdir/docs$release.tar.gz $client/
        err_handler "on creating archive for docs."
        print_ok "[1.1] Archiving for docs completed succesfully."

# archive jboss client folder
cd /backup
cd $jboss
tar -czvf /docs/transfer-temp/$backupdir/jboss$release.tar.gz $client/
        err_handler "on creating archive for jboss client folder."
        print_ok "[1.2] Archiving for jboss client folder completed succesfully."

# archive system htdocs ###################
if  [ $sysdocs == "y" ]; then

if  [ $relst6 = 1 ]; then
cd /www/htdocs/
tar -czvf /docs/transfer-temp/$backupdir/system$release.tar.gz system$release/
	err_handler "on creating archive for system htdocs."
        print_ok "[1.3] Archiving for system htdocs completed succesfully."

elif [ $relxlr = 1 ]; then
cd /www/htdocs/
tar -czvf /docs/transfer-temp/$backupdir/system$release.tar.gz systemxlr8-$release/
	err_handler "on creating archive for system htdocs."
        print_ok "[1.3] Archiving for system htdocs completed succesfully."

fi

fi


# remote transfer
echo ""
echo -e "\033[1;34m [* STEP 2/3 *] Starting transfering files to remote host... \033[0m"
echo ""
$SSHPASS -p '$pass' scp "-o StrictHostKeyChecking=no" "-o UserKnownHostsFile=/dev/null" -C -r /docs/transfer-temp/$backupdir $user@$DEST_IP:/home/$user/
        err_handler "on copying files to remote host."
        print_ok "Files transfered succesfully to remote host: $DEST_IP"


# clean-up and rename client folders to moved-$client
#mv /docs/$client /docs/moved-$client
#mv $jboss/$client $jboss/moved-$client
#rm -rf /docs/transfer-temp/$backupdir

# remote extract
echo ""
echo -e "\033[1;34m [* STEP 3/3 *] Starting extracting files on remote host... \033[0m"
echo ""
if  [ $sysdocs == "y" ]; then
# extract all: docs, jboss and sysdocs
if [[ $relst6 = 1 ]]; then
$SSHPASS -p '$pass' ssh "-o StrictHostKeyChecking=no" "-o UserKnownHostsFile=/dev/null" $user@$DEST_IP "cd /home/$user/$backupdir; tar -xf docs$release.tar.gz -C /docs; tar -xf jboss$release.tar.gz -C /opt/jboss-4.0.2/server; tar -xf system$release.tar.gz -C /www/htdocs;"
        err_handler "on extracting files on remote host."
        print_ok "Files extracted succesfully on remote host: $DEST_IP"
elif [[ $relxlr = 1 ]]; then
$SSHPASS -p '$pass' ssh "-o StrictHostKeyChecking=no" "-o UserKnownHostsFile=/dev/null" $user@$DEST_IP "cd /home/$user/$backupdir; tar -xf docs$release.tar.gz -C /docs; tar -xf jboss$release.tar.gz -C /opt/wildfly-8.0.0/server; tar -xf systemxlr8-$release.tar.gz -C /www/htdocs;"
        err_handler "on extracting files on remote host."
        print_ok "Files extracted succesfully on remote host: $DEST_IP"


else
# extract only docs and jboss
$SSHPASS -p '$pass' ssh "-o StrictHostKeyChecking=no" "-o UserKnownHostsFile=/dev/null" $user@$DEST_IP "cd /home/$user/$backupdir; tar -xf docs$release.tar.gz -C /docs; tar -xf jboss$release.tar.gz -C /opt/jboss-4.0.2/server;"
        err_handler "on extracting files on remote host."
        print_ok "Files extracted succesfully on remote host: $DEST_IP"
fi
fi
}


function exec-time()
{
after=$(date +%s)
elapsed_seconds=$(expr $after - $before)
echo ""
echo -e "\033[1;34m [*] Script execution took:" $(date -d "1970-01-01 $elapsed_seconds sec" +%H:%M:%S)"\033[0m"
}

#############################################################################################################################################
# MAIN SCRIPT LOGIC: call the needed functions here
#############################################################################################################################################

check_client; check_remote_server; transfer_options;

case $transfer in
1 )
    #clean-temp; rsync-move; exec-time;;
    rsync-move; exec-time;;
2 )
    #check-bkp; clean-temp; tar-scp; exec-time;;
    check-bkp; tar-scp; exec-time;;
* ) echo -e "\033[1;0;31m [ERROR]\033[0m: Invalid transfer option. You must choose '1' or '2'";;
esac
#fi
# END
